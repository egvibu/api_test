import json
import subprocess
import shlex
import os

token = "23c48d36ed2d7613ed92faed4c3d45aecfe8d8f4" #os.environ["BOT_TOKEN"]

def getMetadata():
    string = 'curl -X GET "https://online.moysklad.ru/api/remap/1.2/entity/metadata" -H "Authorization: Bearer ' + token + '"'
    args = shlex.split(string)
    jsonData = subprocess.check_output(args)
    output = json.loads(jsonData)

    return output

def getMetaByID(ID):
    string = 'curl -X GET "https://online.moysklad.ru/api/remap/1.2/entity/product/?filter=code=' + ID + '" -H "Authorization: Bearer ' + token + '"'
    #string = 'curl -X GET "https://online.moysklad.ru/api/remap/1.2/entity/product?filter=code=01" -H "Authorization: Bearer ' + token + '"'
    args = shlex.split(string)
    jsonData = subprocess.check_output(args)
    output = json.loads(jsonData)

    return output

print(getMetaByID("01"))
